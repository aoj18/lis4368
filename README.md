> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Application Development

## Arlynn Jensen

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK 
    - Install Tomcat 
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions 

2. [A2 README.md](a2/README.md "My A2 README.md file")


	- MySQL Installation
	- Compiling and Using Servlets
	- Database Connectivity Using Servlets
	- Provide screenshot of the query results 
	- Provide screenshot of a2/index.jsp file
	- Assessment links:
		- [http://localhost:9999/hello](http://localhost:9999/hello/)
		- [http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/index.html)
		- [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)
		- [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)

3. [A3 README.md](a3/README.md "My A3 README.md file")
4. [A4 README.md](a4/README.md "My A4 README.md file")
5. [A5 README.md](a5/README.md "My A5 README.md file")

	