# LIS4368 Advanced Web Application Development

## Arlynn Jensen

### Assignment #2 Requirements:

*Assessment:*

1. [http://localhost:9999/hello/](http://localhost:9999/hello/)
2. [http://localhost:9999/hello/index.html](http://localhost:9999/hello/index.html)
3. [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello) 
4. [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)

#### README.md file should include the following items:

* Assessment links 
* Screenshots (3 minimum): 1) querybook.html, 2) the query results, and 3) a2/index.jsp file 




#### Assignment Screenshots:


*Screenshot of running querybook.html*:
![Querybook.html Screenshot](img/querybook.png)

*Screenshot of running query results*:
![query results Screenshot](img/queryresults.png)

*Screenshot of a2/index.jsp file*:
![a2/index Screenshot](img/a2index1.png)
![a2/index 2 Screenshot](img/a2index2.png)





