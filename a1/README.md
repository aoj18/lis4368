> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Arlynn Jensen

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/SP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running http://localhost:9999
* Link to local lis4368 web app
* Git commands with short description
* Bitbucket repo links: this assignment, and the completed tutorial repo (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates a new Git repository. This command can be used to convert an existing, unversioned project to a Git repository or inilialize a new, empty repository.
2. git status - displays the state of the working directory and the staging area. This command lets you see which changes have been staged, which haven't, and which files aren't being tracked by Git.
3. git add - adds a change in the working directory to the staging area. This command tells Git that you want to include updates to a particular file in the next commit.
4. git commit - captures a snapshot of the project's currently staged changes. Committed snapshots can be thought of as "safe" versions of a project- Git will never change them unless you explicitly ask it to.
5. git push - used to upload local repository content to a remote repository. Pushing is how you transfer commits from your local repository to a remote repository. 
6. git pull - used to fetch and download content from a remote repository and immediately update the local repository to match that content.
7. git merge - join two or more development histories together. This command incorporates changes from the named commits (since the time their histories diverged from the current branch) into the current branch. 

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Screenshot](img/ApacheTomcatScreenshot.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/hellojavascreenshot.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/localhostss.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://aoj18@bitbucket.org/aoj18/bitbucketstationlocations.git "Bitbucket Station Locations")


